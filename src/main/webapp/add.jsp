<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
		 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    
		<title>Insert title here</title>
	</head>
	<body>
		
		 <div class="container mt-4 col-lg-5">
        <div class="card">
            <div class="card-header ">

                <h5>Agregar nuevo Usuario</h5>

            </div>
            <div class="card-body">
                
                <form action="">
                    <label> Nombres </label>                    
                    <br>
                    <input type="text" name="txtnom" class="form-control">
                    <br>
                    <label> Apellidos </label>                    
                    <br>
                    <input type="text" name="txtape" class="form-control">
                    <br>           
                    <input class="btn btn-success" type="submit" name="accion" value="Guardar" >
                    <a class="btn border" href="Controlador?accion=index"> Regresar </a>
                </form>
                
            </div>
        </div>
    </div>
		
		
	</body>
</html>