<%@page import="webservice.User"%>
<%@page import="java.util.List"%>
<%@page import="modelo.UserService"%>



<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>jeje</title>
</head>
<body>

    <div class="container mt-4">
        <div class="card">
            <div class="card-header">
                <a href="Controlador?accion=add" class="btn btn-primary ">Nuevo Usuario</a>
            </div>
            <div class="card-body">
                <table class="table table-hover ">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>NOMBRES</th>
                            <th>APELLIDOS</th>
                            <th>ACCIONES</th>
                             
                        </tr>
                    </thead>
                    <tbody>
                    <%	
                    		UserService user = new UserService();
                    		List<User> datos  = user.listar();
                    		
                    		for(User u : datos){
                    	
                    	%>
                    
                        <tr>
                            <td> <%= u.getId()    %></td>
                            <td> <%= u.getFname() %></td>
                            <td> <%= u.getLname() %></td>
                            <td>
                        		<a href="Controlador?accion=editar&id=<%=u.getId()%>" class="btn btn-warning" > Editar </a>
                        		<a href="Controlador?accion=eliminar&id=<%=u.getId()%>" class="btn btn-danger" > Eliminar </a>
                        	
                        	</td>
                        </tr>
                        
                       <% } %>
                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>


</body>

</html>
