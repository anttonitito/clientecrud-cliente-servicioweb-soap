package modelo;

import java.util.List;

import webservice.Servicios;
import webservice.User;
import webservice.WebserviceServicios;

public class UserService {

	
	
	public static List<User> listar(){
		WebserviceServicios servicio = new WebserviceServicios();
		Servicios port = servicio.getServiciosPort();
		
		return port.listar();
	}
	
	public static String  agregar(String nombres, String apellidos){
		WebserviceServicios servicio = new WebserviceServicios();
		Servicios port = servicio.getServiciosPort();
		
		return port.agregar(nombres, apellidos);
	}
	public static User listarID(int id){
		WebserviceServicios servicio = new WebserviceServicios();
		Servicios port = servicio.getServiciosPort();
		
		return port.listarID(id);
	}
	public static String  editar( int id, String nombres, String apellidos){
		WebserviceServicios servicio = new WebserviceServicios();
		Servicios port = servicio.getServiciosPort();
		
		return port.editar(id, nombres, apellidos);
	}
	
	public static String  elmiminar( int id){
		WebserviceServicios servicio = new WebserviceServicios();
		Servicios port = servicio.getServiciosPort();
		
		return port.eliminar(id);
	}
	
}
