package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.UserService;

@WebServlet("/Controlador")
public class Controlador extends HttpServlet {

	String add="add.jsp";
	String edit="edit.jsp";

	String index="index.jsp";
	String acceso="";
	
	UserService user = new UserService();
	
	
	@Override
	protected void doGet(HttpServletRequest request, javax.servlet.http.HttpServletResponse response)
			throws ServletException, IOException {
	
		response.setContentType("text/html; charset=UTF-8");
		String accion = request.getParameter("accion");
		if (accion.equals("add")) {
			acceso = add;
		}
		else if(accion.equals("Guardar")){
			String nom = request.getParameter("txtnom");
			String ape = request.getParameter("txtape");
			
			user.agregar(nom, ape);
			acceso = index;
		}
		else if(accion.equals("editar")) {
			acceso = edit;
			request.setAttribute("iduser", request.getParameter("id"));
			
		}else if(accion.equals("Actualizar")){
			String nom = request.getParameter("txtnom");
			String ape = request.getParameter("txtape");
			int id = Integer.parseInt(request.getParameter("txtid")); 
			user.editar(id,nom, ape);
			
			acceso = index;
		}else if(accion.equals("eliminar")){
			int id = Integer.parseInt( (String) request.getParameter("id")) ;
			user.elmiminar(id);
			acceso = index;
		}else {
			acceso=index;
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(acceso);
		dispatcher.forward(request, response);
		
	}
	
	
	public void processRequest( HttpServletRequest request, HttpServletResponse response )   {
		
		
	
	}
	
	
	
}
